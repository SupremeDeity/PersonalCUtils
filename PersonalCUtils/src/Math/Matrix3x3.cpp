#include "pcpch.h"
#include "Matrix3x3.h"

namespace PCU
{
	Matrix3x3::Matrix3x3()
	{
		this->m00 = 0.0f; this->m10 = 0.0f; this->m20 = 0.0f;
		this->m01 = 0.0f; this->m11 = 0.0f; this->m21 = 0.0f;
		this->m02 = 0.0f; this->m12 = 0.0f; this->m22 = 0.0f;
	}

	Matrix3x3::Matrix3x3(Vector3f c00, Vector3f c01, Vector3f c02)
	{
		PCU_PROFILE_FUNCTION();
		this->m00 = c00.x; this->m10 = c00.y; this->m20 = c00.z;
		this->m01 = c01.x; this->m11 = c01.y; this->m21 = c01.z;
		this->m02 = c02.x; this->m12 = c02.y; this->m22 = c02.z;
	}

	void Matrix3x3::Set(int row, int column, float val)
	{
		switch (column)
		{
		case 0:
			switch (row)
			{
			case 0:
				m00 = val;
				break;
			case 1:
				m10 = val;
				break;
			case 2:
				m20 = val;
				break;
			}
		case 1:
			switch (row)
			{
			case 0:
				m01 = val;
				break;
			case 1:
				m11 = val;
				break;
			case 2:
				m21 = val;
				break;
			}
		case 2:
			switch (column)
			{
			case 0:
				m02 = val;
				break;
			case 1:
				m12 = val;
				break;
			case 2:
				m22 = val;
				break;
			}
		}
	}

	float Matrix3x3::Get(int row, int column)
	{
		switch (column)
		{
		case 0:
			switch (row)
			{
			case 0:
				return m00;
				break;
			case 1:
				return m10;
				break;
			case 2:
				return m20;
				break;
			}
		case 1:
			switch (row)
			{
			case 0:
				return m01;
				break;
			case 1:
				return m11;
				break;
			case 2:
				return m21;
				break;
			}
		case 2:
			switch (column)
			{
			case 0:
				return m02;
				break;
			case 1:
				return m12;
				break;
			case 2:
				return m22;
				break;
			}
		}
		return float();
	}

	Vector3f Matrix3x3::GetRow(int index)
	{
		switch (index)
		{
		case 0:
			return Vector3f(m00, m10, m20);
		case 1:
			return Vector3f(m01, m11, m21);
		case 2:
			return Vector3f(m02, m12, m22);
		default:
			return Vector3f();
		}
	}

	Vector3f Matrix3x3::GetColumn(int index)
	{
		switch (index)
		{
		case 0:
			return Vector3f(m00, m01, m02);
		case 1:
			return Vector3f(m10, m11, m12);
		case 2:
			return Vector3f(m20, m21, m22);
		default:
			return Vector3f();
		}
	}

	std::string Matrix3x3::to_string(Matrix3x3 in)
	{
		PCU_PROFILE_FUNCTION();
		return Vector3f::to_string(in.GetRow(0)) + "\n" + Vector3f::to_string(in.GetRow(1)) + "\n" + Vector3f::to_string(in.GetRow(2));
	}

	// Check If Values are same. For Checking if instances are same refer to operator==
	bool Matrix3x3::Equals(Matrix3x3 matrix)
	{
		if ((GetRow(0).Equals(matrix.GetRow(0))) && ((GetRow(1).Equals(matrix.GetRow(1))) && ((GetRow(2).Equals(matrix.GetRow(2)))))) { return true; }
		else { return false; }
	}

	Matrix3x3 Matrix3x3::operator*(float val)
	{
		return Matrix3x3(
			Vector3f(m00 * val, m10 * val, m20 * val),
			Vector3f(m01 * val, m11 * val, m21 * val),
			Vector3f(m02 * val, m12 * val, m22 * val)
		);
	}

	Matrix3x3 Matrix3x3::operator*(Matrix3x3 m)
	{
		return Matrix3x3(
			Vector3f(m00 * m.m00 + m10 * m.m01 + m20 * m.m02, m00 * m.m10 + m10 * m.m11 + m20 * m.m12, m00 * m.m20 + m10 * m.m21 + m20 * m.m22),
			Vector3f(m01 * m.m00 + m11 * m.m01 + m21 * m.m02, m01 * m.m10 + m11 * m.m11 + m21 * m.m12, m01 * m.m20 + m11 * m.m21 + m21 * m.m22),
			Vector3f(m02 * m.m00 + m12 * m.m01 + m22 * m.m02, m02 * m.m10 + m12 * m.m11 + m22 * m.m12, m02 * m.m20 + m12 * m.m21 + m22 * m.m22)
		);
	}

	Matrix3x3 Matrix3x3::operator-(float val)
	{
		return Matrix3x3(
			Vector3f(m00 - val, m10 - val, m20 - val),
			Vector3f(m01 - val, m11 - val, m21 - val),
			Vector3f(m02 - val, m12 - val, m22 - val)
		);
	}

	Matrix3x3 Matrix3x3::operator-(Matrix3x3 m)
	{
		return Matrix3x3(
			Vector3f(m00 - m.m00, m10 - m.m10, m20 - m.m20),
			Vector3f(m01 - m.m01, m11 - m.m11, m21 - m.m21),
			Vector3f(m02 - m.m02, m12 - m.m12, m22 - m.m22)
		);
	}

	Matrix3x3 Matrix3x3::operator+(float val)
	{
		return Matrix3x3(
			Vector3f(m00 + val, m10 + val, m20 + val),
			Vector3f(m01 + val, m11 + val, m21 + val),
			Vector3f(m02 + val, m12 + val, m22 + val)
		);
	}

	Matrix3x3 Matrix3x3::operator+(Matrix3x3 m)
	{
		return Matrix3x3(
			Vector3f(m00 + m.m00, m10 + m.m10, m20 + m.m20),
			Vector3f(m01 + m.m01, m11 + m.m11, m21 + m.m21),
			Vector3f(m02 + m.m02, m12 + m.m12, m22 + m.m22)
		);
	}

	Matrix3x3 Matrix3x3::operator/(float val)
	{
		return Matrix3x3(
			Vector3f(m00 / val, m10 / val, m20 / val),
			Vector3f(m01 / val, m11 / val, m21 / val),
			Vector3f(m02 / val, m12 / val, m22 / val)
		);
	}

	Matrix3x3 Matrix3x3::operator/(Matrix3x3 m)
	{
		return Matrix3x3(
			Vector3f(m00 / m.m00, m10 / m.m10, m20 / m.m20),
			Vector3f(m01 / m.m01, m11 / m.m11, m21 / m.m21),
			Vector3f(m02 / m.m02, m12 / m.m12, m22 / m.m22)
		);
	}

	// Check If Instances are same. For Checking if values are same refer to Equals()
	bool Matrix3x3::operator==(Matrix3x3* fvec)
	{
		if (fvec == this) { return true; }
		else { return false; }
	}
}