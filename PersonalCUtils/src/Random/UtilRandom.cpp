#include "pcpch.h"
#include "UtilRandom.h"

namespace PCU
{
	int SeededRandom(int min, int max)
	{
		srand((unsigned int)time(NULL));
		int seed = rand() % max + min;
		return seed;
	}
}