#pragma once
#include "Vector3f.h"

namespace PCU
{
	struct Matrix3x3
	{
	public:
		Matrix3x3();
		Matrix3x3(Vector3f c00, Vector3f c01, Vector3f c02);

		void Set(int row, int column, float val);
		float Get(int row, int column);

		Vector3f GetColumn(int index);
		Vector3f GetRow(int index);

		static std::string to_string(Matrix3x3 in);

		// checks if values are same
		bool Equals(Matrix3x3 matrix);

		Matrix3x3 operator*(float val);
		Matrix3x3 operator*(Matrix3x3 m);

		Matrix3x3 operator-(float val);
		Matrix3x3 operator-(Matrix3x3 m);

		Matrix3x3 operator+(float val);
		Matrix3x3 operator+(Matrix3x3 m);

		Matrix3x3 operator/(float val);
		Matrix3x3 operator/(Matrix3x3 m);

		// checks if instances are same
		bool operator==(Matrix3x3* fvec);
	private:

		/*  ---------------------- Matrix Reference ------------------------
		M-> MATRIX, First digit: row, second digit: column

							Columns [top to bottom]
								| m00 m10 m20 |
		 Rows[left to right]	| m01 m11 m21 |
								| m02 m12 m22 |
		*/
		// Matrix 

		float m00;
		float m10;
		float m20;

		float m01;
		float m11;
		float m21;

		float m02;
		float m12;
		float m22;

	};
}