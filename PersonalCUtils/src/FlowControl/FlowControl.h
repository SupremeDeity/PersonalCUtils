#pragma once


namespace PCU {

	static bool isCalled = false;

	// DoOnce
	template<typename T>
	void DoOnce (T (*fcnPtr)());
	

	// Template Functions

	/** Executes the Function Once, multiple calls wont work. function must not have a parameter.
		Call PCUtils::StateDoOnce(bool state) to change state;
	**/
	template<typename T>
	void DoOnce (T (*fcnPtr)())
	{

		if (isCalled == false) {
			isCalled = true;
			fcnPtr ( );
		}
	}

	/** internal_f. 
	Sets DoOnce Function's New State.
	**/
	void StateDoOnce (bool state)
	{
		isCalled = state;
	}

	// Gets Each Item In Array, Returns current index and data according to the current index.
	template<typename F>
	void ForEach (F Array[], int &index, F &Data)
	{
		for (int x = 0; x < 5; x++) {
			index = x;
			Data = Array[x];
		}
	}

	
}