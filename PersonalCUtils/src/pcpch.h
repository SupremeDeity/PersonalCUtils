#pragma once

// Standard cpp
#include <iostream>
#include <stdio.h>
#include <sstream>
#include <fstream>
#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <functional>
#include <memory>
#include <utility>
#include <string>
#include <ctime>
#include <time.h>
#include <chrono>
#include <filesystem>
#include <thread>
#include <cmath>
#include <tuple>
#include <mutex>
#include <array>

#include "Profiling\Instrumentor.h"