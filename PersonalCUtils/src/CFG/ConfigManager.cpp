#include "pcpch.h"
#include "ConfigManager.h"

namespace PCU
{
	ConfigManager::ConfigManager(std::string filePath)
		: m_FilePath(filePath)
	{
		PCU_PROFILE_FUNCTION();
		FillBuffer();
		OrderBuffer();
	}

	bool ConfigManager::GetBoolValue(std::string section, std::string key, bool & value)
	{
		std::string valueToConvert;
		if (!GetValue(section, key, valueToConvert))
		{
			return false;
		}

		// We need to convert to string ourselves.
		if (valueToConvert.compare("true") || valueToConvert.compare("True") || valueToConvert.compare("1"))
		{
			value = true;
			return true;
		}

		else if (valueToConvert.compare("false") || valueToConvert.compare("False") || valueToConvert.compare("0"))
		{
			value = false;
			return true;
		}

		return false;
	}

	bool ConfigManager::GetIntValue(std::string section, std::string key, int & value)
	{
		std::string valueToConvert;
		if (!GetValue(section, key, valueToConvert))
		{
			return false;
		}

		char* endptr;
		int i = std::strtol(valueToConvert.c_str(), &endptr, 0);
		if ((valueToConvert == endptr) || (i == ERANGE))
		{
			return false;
		}
		else
		{
			value = i;
			return true;
		}
		return false;
	}

	bool ConfigManager::GetFloatValue(std::string section, std::string key, float & value)
	{
		std::string valueToConvert;
		if (!GetValue(section, key, valueToConvert))
		{
			return false;
		}

		char* endptr;
		float i = std::strtof(valueToConvert.c_str(), &endptr);
		if ((valueToConvert == endptr) || (i == ERANGE))
		{
			return false;
		}
		else
		{
			value = i;
			return true;
		}
		return false;
	}

	bool ConfigManager::SetValue(std::string section, std::string key, std::string value)
	{
		// Technically this will always return true.
		m_OrderedBuffer[section].emplace(key, value);
		return true;
	}

	bool ConfigManager::Flush()
	{
		std::fstream stream("test.txt", std::ios::out);

		if (!stream.is_open()) { std::cout << "Flush: Failed to open Config File." << std::endl; return false; }

		for (auto x : m_OrderedBuffer)
		{
			// Write Section name
			stream << x.first << std::endl;

			for (auto y : x.second)
			{
				stream << y.first << "=" << y.second << std::endl;
			}

			// Extra new line for aesthethics
			stream << std::endl;
		}

		return true;
	}

	// ######################################################################
	// ################### Private Member Functions #########################
	// ######################################################################
	void ConfigManager::FillBuffer()
	{
		PCU_PROFILE_FUNCTION();
		std::fstream stream(m_FilePath, std::fstream::in);

		if (!stream.is_open()) { std::cout << "Failed to open Config File" << std::endl; }

		std::string line;
		while (std::getline(stream, line))
		{
			m_FileBuffer.push_back(line);
		}
	}

	void ConfigManager::OrderBuffer()
	{
		PCU_PROFILE_FUNCTION();
		std::string section = "";
		int SectionIndex = 0;

		for (std::string& line : m_FileBuffer)
		{
			// We Skip To the next iteration if we have found a section.
			if (line.find("[") != std::string::npos)
			{
				section = line;
				continue;
			}

			// Only Runs if Section is already set
			if (!section.empty())
			{
				if (line.find("=") != std::string::npos)
				{
					// Separate Value & Key
					int EqualIndex = line.find("=");

					std::string Key = line.substr(0, EqualIndex);
					std::string Value = line.substr((long long int)EqualIndex + 1, line.size() - 1);

					// This line will put the value in to the correct section, if the section does not exist, it will create it.
					m_OrderedBuffer[section].emplace(Key, Value);
				}
			}
		}
	}

	bool ConfigManager::GetValue(std::string section, std::string key, std::string& value)
	{
		PCU_PROFILE_FUNCTION();
		if (m_OrderedBuffer.contains(section))
		{
			if (m_OrderedBuffer[section].contains(key))
			{
				value = m_OrderedBuffer[section][key];
				return true;
			}
			else { return false; }
		}
		else { return false; }
		return false;
	}
}