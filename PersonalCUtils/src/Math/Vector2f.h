#pragma once

namespace PCU
{
	struct Vector2f
	{
	public:
		Vector2f(float x, float y);
		Vector2f();

		void Set(float x, float y);

		float FindMagnitude(Vector2f* vec);

		Vector2f normalize();
		float normalizeX();
		float normalizeY();

		static float Dot(Vector2f fvec, Vector2f svec);

		static Vector2f Lerp(Vector2f fvec, Vector2f svec, float percentage);

		static std::string to_string(Vector2f fvec);

		// Checks if values are same
		bool Equals(Vector2f fvec);

		// checks if instances are same
		bool operator==(Vector2f* vec);

		Vector2f operator +(Vector2f& val);
		Vector2f operator -(Vector2f& val);
		Vector2f operator /(Vector2f& val);
		Vector2f operator *(Vector2f& val);

		Vector2f operator *(float val);
		Vector2f operator -(float val);
		Vector2f operator +(float val);
		Vector2f operator /(float val);
	public:
		float x, y, length;
	};
}