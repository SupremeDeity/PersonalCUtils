#include <iostream>
// PCUtils
#include <Logger/Logger.h>
#include <Time/Timer.h>
#include <CFG/ConfigManager.h>
#include <Math/Vector3f.h>
#include <Math/Matrix3x3.h>
#include <Profiling/Instrumentor.h>
#include <mutex>

using namespace PCU;

int main()
{
	// Profiling
	PCU_PROFILE_BEGIN_SESSION("PCU-Profile", "PCU-Profile.json");
	PCU_PROFILE_FUNCTION();

	// Config Manager
	ConfigManager* manager = new ConfigManager("test.ini");

	std::string infoPath, warningPath, errorPath;

	manager->GetValue("[LogPaths]", "InfoPath", infoPath);
	manager->GetValue("[LogPaths]", "WarningPath", warningPath);
	manager->GetValue("[LogPaths]", "ErrorPath", errorPath);

	// Logger
	Logger* logger = new Logger({infoPath, warningPath, errorPath});
	logger->SetSeverityLevel(Severity::INFO); // Print Everything

	// Optional Stuff
	logger->SetName("PCU_MAINLOG");
	logger->SetAppend(true);
	logger->SetUseDT(true);
	logger->SetUseColors(true);
	logger->SetPattern("{Severity}: [{LoggerName}]: {Str}\n");
	// Vector3F
	Vector3f vec = Vector3f(1.0f, 2.0f, 3.0f);

	// Matrix
	Matrix3x3 mat = Matrix3x3(Vector3f(2.0f, 0.5f, 0.3f), Vector3f(1.0f, 5.0f, 3.0f), Vector3f(0.1f, 2.0f, 3.0f));

	// Example
	{
		Timer* timer = new Timer();
		logger->Log(Severity::INFO, "Initialized Components {0}, {1}, {2}, {3}, {4}", "[Logger]", "[ConfigManager]", "[Vector3f]", "[Matrix3f]", "[Timer]");
		logger->Log(Severity::WARNING, "Vec3: " + Vector3f::to_string(vec));
		logger->Log(Severity::ERROR, "Mat3x3: \n" + Matrix3x3::to_string(mat) + "\n");

		// print to files
		logger->flush();
		delete(timer);
	}
	PCU_PROFILE_END_SESSION();
	std::cin.get();

	return 0;
}


/*#include <iostream>
#include <thread>
#include <mutex>
#include <Logger\Logger.h>
#include <Time\Timer.h>

#define NO_OF_LOGS 100000
#define THREADS 1

std::mutex mtx;

using namespace PCU;

Logger* logger = new Logger("test.txt");
void Log()
{
	mtx.lock();
	for (int x = 0; x < (NO_OF_LOGS / THREADS); x++)
	{
		logger->Log(Severity::ERROR, "THREAD!");
	}
	mtx.unlock();
}


int main()
{
	logger->SetPattern("{LoggerName}: {Severity}: {Str} \n");
	logger->SetAddToFileBuffer(true);
	logger->SetPrint(false);
	logger->SetUseColors(false); // To increase performance
	
	std::array<std::thread*, THREADS> threads;
	
	threads[0] = new std::thread(Log);
	threads[1] = new std::thread(Log);
	threads[2] = new std::thread(Log);
	threads[3] = new std::thread(Log); 

	Timer* timer = new Timer();
	//Log();
	
	threads[0]->join();
	threads[1]->join();
	threads[2]->join();
	threads[3]->join();

	logger->flush();
	delete(timer);

	std::cin.get();
	return 0;
	
}*/