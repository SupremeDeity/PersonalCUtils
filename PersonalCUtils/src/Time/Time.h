//								--	SUPREMEDEITY CTIME WRAPPER  --
// This wrapper uses the standard ctime library
// MSVC 10 And Above give unsafe warnings/errors so _CRT_SECURE_NO_WARNINGS has to be defined
#pragma once
namespace PCU {
	class Time {
	public:
		static int GetSecond();
		static int GetMinute();
		static int GetHour();
		static int GetYear();
		static int GetMonth();
		static int GetDay();
		static int GetWDay();
		static int GetYDday();
		static int GetDSavings();
	};
}