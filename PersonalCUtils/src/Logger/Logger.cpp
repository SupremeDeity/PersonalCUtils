#include "pcpch.h"
#include "Logger.h"
#include "Time/Time.h"
#include "Time/Timer.h"


namespace PCU
{
	LogPaths::LogPaths(const std::string& info, const std::string& warning, const std::string& error)
		: m_info(info), m_error(error), m_warning(warning)
	{
		m_InfoDir = m_info.parent_path();
		m_WarningDir = m_warning.parent_path();
		m_ErrorDir = m_error.parent_path();
	}

	Logger::Logger() {}

	Logger::Logger(LogPaths paths)
	{
		PCU_PROFILE_FUNCTION();

		data.Paths = paths;
	}

	Logger::Logger(const std::string& path)
	{
		data.Paths = { path , path , path };
	}

	Logger::~Logger()
	{
		data.Buffer.clear();
		data.Buffer.shrink_to_fit();
	}

	std::string Logger::FormatString(const std::string& severity, const std::string& str)
	{
		std::string fmtStr = fmt::format(data.Pattern,
				  fmt::arg("Str", str),
				  fmt::arg("LoggerName", data.Name),
				  fmt::arg("Severity", severity),
				  fmt::arg("Tid", std::to_string(std::hash<std::thread::id>{}(std::this_thread::get_id())))
		);

		return fmtStr;
	}

	int Logger::GetColor(Severity severity)
	{
		switch (severity)
		{
		case Severity::ERROR:
			return 4;
		case Severity::WARNING:
			return 6;
		case Severity::INFO:
			return 7;
		default:
			break;
		}
	}

	void Logger::Print(const std::string& str, Severity severity)
	{
		SET_COLOR(severity);
		fmt::print(str);
		SET_COLOR_DEFAULT(); // Set Info = default color
	}

	void Logger::flush()
	{
		PCU_PROFILE_FUNCTION();

		{
			
			PCU_PROFILE_SCOPE("Create Directories");
			if (!std::filesystem::exists(data.Paths.GetInfoDir())) { std::filesystem::create_directories(data.Paths.GetInfoPath().parent_path()); }
			if (!std::filesystem::exists(data.Paths.GetWarningDir())) { std::filesystem::create_directories(data.Paths.GetWarningDir()); }
			if (!std::filesystem::exists(data.Paths.GetErrorDir())) { std::filesystem::create_directories(data.Paths.GetErrorDir()); }
		}
		std::stringstream infoStream;
		std::stringstream warningStream;
		std::stringstream errorStream;

		{
			PCU_PROFILE_SCOPE("BufferString Stream Fill");
			for (auto line : data.Buffer)
			{
				if (line.first == Severity::INFO) { infoStream << line.second; }
				if (line.first == Severity::WARNING) { warningStream << line.second; }
				if (line.first == Severity::ERROR) { errorStream << line.second; }
			}
		}
		{
			PCU_PROFILE_SCOPE("Write to File");

			// Output file stream
			std::ofstream stream;
			if(infoStream.rdbuf()->in_avail() != 0) {
				stream.open(data.Paths.GetInfoPath(), std::ios::out | ((data.Append) ? std::ios::app : std::ios::trunc));

				stream << infoStream.str();
				stream.clear();
				stream.close();
			}

			if (warningStream.rdbuf()->in_avail() != 0)
			{
				stream.open(data.Paths.GetWarningPath(), std::ios::out | ((data.Append) ? std::ios::app : std::ios::trunc));

				stream << warningStream.str();
				stream.clear();
				stream.close();
			}

			if (errorStream.rdbuf()->in_avail() != 0)
			{
				stream.open(data.Paths.GetErrorPath(), std::ios::out | ((data.Append) ? std::ios::app : std::ios::trunc));

				stream << errorStream.str();
				stream.close();
			}

		}
		{
			PCU_PROFILE_SCOPE("Buffer Empty");
			data.Buffer.clear();
			data.Buffer.shrink_to_fit();
		}
	}

	std::string Logger::GetLevelStr(Severity lvl)
	{
		switch (lvl)
		{
		case Severity::INFO:		return "INFO";
		case Severity::WARNING:		return "WARNING";
		case Severity::ERROR:		return "ERROR";
		default:					break;
		}
		return std::string();
	}

}