﻿#include "pcpch.h"
#include "Timer.h"

namespace PCU {

	Timer::Timer()
	{
		PCU_PROFILE_FUNCTION();
		m_Start = std::chrono::high_resolution_clock::now();

	}

	Timer::~Timer()
	{
		PCU_PROFILE_FUNCTION();
		m_End = std::chrono::high_resolution_clock::now();

		long long start = std::chrono::time_point_cast<std::chrono::microseconds>(m_Start).time_since_epoch().count();
		long long end = std::chrono::time_point_cast<std::chrono::microseconds>(m_End).time_since_epoch().count();

		float duration = end - start;

		std::cout << "Took: " << duration << " microsecs" << std::endl;
		std::cout << "Took: " << duration / 1000 << " ms" << std::endl;
		std::cout << "Took: " << duration / 1e+6 << " s" << std::endl;

	}

}