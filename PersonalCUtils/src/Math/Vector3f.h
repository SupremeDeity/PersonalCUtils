#pragma once
#include <string>

namespace PCU
{
	struct Vector3f
	{
	public:
		Vector3f(float x, float y, float z);
		Vector3f();

		void Set(float x, float y, float z);

		float FindMagnitude(Vector3f* vec);

		Vector3f normalize();
		float normalizeX();
		float normalizeY();
		float normalizeZ();

		static Vector3f Cross(Vector3f fvec, Vector3f svec);
		static float Dot(Vector3f fvec, Vector3f svec);

		static Vector3f Lerp(Vector3f fvec, Vector3f svec, float percentage);

		static std::string to_string(Vector3f fvec);

		bool Equals(Vector3f fvec);

		bool operator==(Vector3f* vec);

		Vector3f operator +(Vector3f& val);
		Vector3f operator -(Vector3f& val);
		Vector3f operator /(Vector3f& val);
		Vector3f operator *(Vector3f& val);

		Vector3f operator *(float val);
		Vector3f operator -(float val);
		Vector3f operator +(float val);
		Vector3f operator /(float val);
	public:
		float x, y, z, length;
	};
}