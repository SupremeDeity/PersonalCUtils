#pragma once
#include <string>
#include <unordered_map>
#include <unordered_set>

// TODO: Add support for comments!!
namespace PCU
{
	class ConfigManager
	{
	public:
		ConfigManager(std::string filePath);

		// Does the necessary conversions and returns the value if they exist. will return false if conversion was unsuccesful, section / key does not exist

		bool GetBoolValue(std::string section, std::string key, bool& value);
		bool GetIntValue(std::string section, std::string key, int& value);
		bool GetFloatValue(std::string section, std::string key, float& value);

		/* Get Value writes to the provided value variable. Use this if you dont want any conversions.
			returns false if section was not found, or if key was not found.
		*/
		bool GetValue(std::string section, std::string key, std::string& value);

		/* This creates the section if it does not exist.
			returns if value was succesfully set, technically always returns true. DOES NOT WRITE TO FILE. SEE flush()
			to write changes to file.
		*/
		bool SetValue(std::string section, std::string key, std::string value);

		

		/*Flush Writes to file. Returns true if the operation was successfull.
			BEWARE!! This function will get rid of anything that does not belong to a section and is not a key value pair. Including Comments.
		*/
		bool Flush();

	private:
		void FillBuffer();
		void OrderBuffer();

	private:
		std::string m_FilePath;
		// Section, Value, Key
		std::unordered_map<std::string, std::unordered_map<std::string, std::string>> m_OrderedBuffer;
		std::vector<std::string> m_FileBuffer;
	};
}