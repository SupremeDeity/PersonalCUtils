#pragma once

namespace PCU
{
	class Timer 
	{
	public: 
		Timer();
		~Timer();
	private:
		std::chrono::high_resolution_clock::time_point m_Start;

		std::chrono::high_resolution_clock::time_point m_End;
	};
}

