# PersonalCUtils
Personal C++ Utility Library

![PersonaCUtils](/Resources/Branding/PersonalCUtils/PCU-Black.png?raw=true "PersonalCUtils")

## Pre-Existing Features
- Math Library (WIP)
- Logger
- Timer
- Configuration Manager

### Maths
- Matrix with basic operations
- Vector 2 till Vector 4 with Dot Product and Lerp functionality.

### Logger 
- Severity Level Based Logging
- File Support for each indiviual Severity Level
- Custom Pattern Support

### Timer
- Basic Timer Functionality to test function execution completion times.

### Configuration Manager
- Basic Configuration Manager With Read And Write Capabilities