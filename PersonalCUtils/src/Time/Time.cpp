#include "pcpch.h"
#include "Time.h"

namespace PCU
{
	int Time::GetSecond()
	{
		time_t tt = time(NULL);
		tm t_Ptr;
		localtime_s(&t_Ptr, &tt);

		return t_Ptr.tm_sec;
	}

	int Time::GetMinute()
	{
		time_t tt = time(NULL);
		tm t_Ptr;
		localtime_s(&t_Ptr, &tt);

		return t_Ptr.tm_min;
	}

	int Time::GetHour()
	{
		time_t tt = time(NULL);
		tm t_Ptr;
		localtime_s(&t_Ptr, &tt);

		return t_Ptr.tm_hour;
	}

	int Time::GetYear()
	{
		time_t tt = time(NULL);
		tm t_Ptr;
		localtime_s(&t_Ptr, &tt);

		return (t_Ptr.tm_year) + 1900;
	}

	int Time::GetMonth()
	{
		time_t tt = time(NULL);
		tm t_Ptr;
		localtime_s(&t_Ptr, &tt);

		return (t_Ptr.tm_mon) + 1;
	}

	int Time::GetDay()
	{
		time_t tt = time(NULL);
		tm t_Ptr;
		localtime_s(&t_Ptr, &tt);

		return t_Ptr.tm_mday;
	}

	int Time::GetWDay()
	{
		time_t tt = time(NULL);
		tm t_Ptr;
		localtime_s(&t_Ptr, &tt);

		return t_Ptr.tm_wday;
	}

	int Time::GetYDday()
	{
		time_t tt = time(NULL);
		tm t_Ptr;
		localtime_s(&t_Ptr, &tt);

		return t_Ptr.tm_yday;
	}

	int Time::GetDSavings()
	{
		time_t tt = time(NULL);
		tm t_Ptr;
		localtime_s(&t_Ptr, &tt);

		return t_Ptr.tm_isdst;
	}
}