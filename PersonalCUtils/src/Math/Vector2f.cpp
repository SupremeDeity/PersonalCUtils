#include "pcpch.h"
#include "Vector2f.h"
#include <algorithm>

namespace PCU
{
	Vector2f::Vector2f(float x, float y)
	{
		this->x = x;
		this->y = y;

		this->length = FindMagnitude(this);
	}
	Vector2f::Vector2f() : x(0.0f), y(0.0f), length(0.0f)
	{}

	void Vector2f::Set(float x, float y)
	{
		this->x = x;
		this->y = y;
	}

	float Vector2f::FindMagnitude(Vector2f* vec)
	{
		return sqrt(Dot(*vec, *vec));
	}

	Vector2f Vector2f::normalize()
	{
		return Vector2f(x / length, y / length);
	}

	float Vector2f::normalizeX()
	{
		return x / length;
	}

	float Vector2f::normalizeY()
	{
		return y / length;
	}

	float Vector2f::Dot(Vector2f fvec, Vector2f svec)
	{
		return fvec.x * svec.x + fvec.y * svec.y;
	}

	Vector2f Vector2f::Lerp(Vector2f fvec, Vector2f svec, float percentage)
	{
		// Clamp To 1.0f
		percentage = percentage > 1.0f ? 1.0f : percentage;

		return Vector2f(
			fvec.x + (svec.x - fvec.x) * percentage,
			fvec.y + (svec.y - fvec.y) * percentage
		);
	}

	std::string Vector2f::to_string(Vector2f fvec)
	{
		return "(" + std::to_string(fvec.x) + ", " + std::to_string(fvec.y) + ")";
	}

	bool Vector2f::Equals(Vector2f fvec)
	{
		if (x == fvec.x && y == fvec.y) { return true; }
		else { return false; }
	}

	bool Vector2f::operator==(Vector2f* vec)
	{
		if (vec == this)
		{
			return true;
		}

		else { return false; }
	}

	Vector2f Vector2f::operator+(Vector2f& val)
	{
		return Vector2f(x + val.x, y + val.y);
	}

	Vector2f Vector2f::operator-(Vector2f& val)
	{
		return Vector2f(x - val.x, y - val.y);
	}

	Vector2f Vector2f::operator/(Vector2f& val)
	{
		return Vector2f(x / val.x, y / val.y);
	}

	Vector2f Vector2f::operator*(Vector2f& val)
	{
		return Vector2f(x * val.x, y * val.y);
	}

	Vector2f Vector2f::operator*(float val)
	{
		return Vector2f(x * val, y * val);
	}

	Vector2f Vector2f::operator/(float val)
	{
		return Vector2f(x / val, y / val);
	}

	Vector2f Vector2f::operator+(float val)
	{
		return Vector2f(x + val, y + val);
	}

	Vector2f Vector2f::operator-(float val)
	{
		return Vector2f(x - val, y - val);
	}
}