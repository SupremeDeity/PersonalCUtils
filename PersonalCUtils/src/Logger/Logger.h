#include "pcpch.h"
#include "fmt/core.h"
#include <filesystem>

#ifdef PLATFORM_WINDOWS
#include <Windows.h>
// BECAUSE WINDOWS.H IS STUPID
#undef ERROR

#define SET_COLOR(lvl) if(lvl != Severity::INFO && data.UseColors) { SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), GetColor(lvl)); }

#define SET_COLOR_DEFAULT() if(data.UseColors) {SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 7);}

#else
#define SET_COLOR(lvl) 
#define SET_COLOR_DEFAULT(lvl) 
#endif // PLATFORM_WINDOWS


namespace PCU
{
	enum class Severity
	{
		ERROR,
		WARNING,
		INFO
	};

	struct LogPaths
	{
	public:
		LogPaths(const std::string& info, const std::string& warning, const std::string& error);

		inline const std::filesystem::path& GetInfoPath() const { return m_info; }
		inline const std::filesystem::path& GetWarningPath() const { return m_warning; }
		inline const std::filesystem::path& GetErrorPath() const { return m_error; }

		inline const std::filesystem::path& GetInfoDir() const { return m_InfoDir; }
		inline const std::filesystem::path& GetWarningDir() const { return m_WarningDir; }
		inline const std::filesystem::path& GetErrorDir() const { return m_ErrorDir; }

	private:
		std::filesystem::path m_info;
		std::filesystem::path m_warning;
		std::filesystem::path m_error;

		std::filesystem::path m_InfoDir;
		std::filesystem::path m_WarningDir;
		std::filesystem::path m_ErrorDir;
	};

	struct LoggerData
	{
		LoggerData() : Paths("", "", ""), LoggingLevel(Severity::INFO) {}
		~LoggerData() = default;

		LogPaths Paths;
		std::string Name = "PCULogger";
		bool UseDateTime = false;
		bool UseColors = true;
		bool Append = false;
		bool toScreen = true;
		bool toFileBuffer = true;
		Severity LoggingLevel;
		std::vector<std::pair<Severity, std::string>> Buffer;
		std::string Pattern = "{severity}: {str}\n";
	};

	class Logger
	{
	public:

	public:
		Logger();
		Logger(LogPaths paths);
		Logger(const std::string& path);
		~Logger();

		// Logs To Screen
		template<typename... Args>
		void Log(Severity lvl, const std::string& str, Args&& ... args);

		void flush();
		std::pair<Severity, std::string>* GetBuffer() { return data.Buffer.data(); }

		inline void SetSeverityLevel(Severity lvl) { data.LoggingLevel = lvl; }
		inline void SetPattern(const std::string& pattern) { data.Pattern = pattern; }
		inline void SetName(const std::string& name) { data.Name = name; }
		inline void SetAppend(bool append) { data.Append = append; }
		inline void SetUseDT(bool use) { data.UseDateTime = use; }
		inline void SetUseColors(bool use) { data.UseColors = use; }
		inline void SetPrint(bool print) { data.toScreen = print; }
		inline void SetAddToFileBuffer(bool addToBuffer) { data.toFileBuffer = addToBuffer; }

	private:
		std::string FormatString(const std::string& severity, const std::string& str);
		int GetColor(Severity severity);
		void Print(const std::string& str, Severity severity);

		static std::string GetLevelStr(Severity lvl);
	private:
		LoggerData data;
	};

	// ############################################################################
	template<typename... Args>
	inline void Logger::Log(Severity lvl, const std::string& str, Args && ... args)
	{
		{
			
			PCU_PROFILE_FUNCTION();

			std::string fmtString = fmt::format(FormatString(GetLevelStr(lvl), str), std::forward<Args>(args)...);

			if (data.toScreen)
			{
				if (lvl <= data.LoggingLevel)
				{
					Print(fmtString, lvl);
				}
			}
			if (data.toFileBuffer)
			{
				data.Buffer.push_back(std::make_pair(lvl, fmtString));
			}
		}
	}

	// #############################################################################

}

 