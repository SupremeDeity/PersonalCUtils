#include "pcpch.h"
#include "Vector4f.h"

namespace PCU
{
	Vector4f::Vector4f(float x, float y, float z, float w)
	{
		this->x = x;
		this->y = y;
		this->z = z;
		this->w = w;

		this->length = FindMagnitude(this);
	}
	Vector4f::Vector4f() : x(0.0f), y(0.0f), z(0.0f), w(0.0f), length(0.0f)
	{}

	void Vector4f::Set(float x, float y, float z, float w)
	{
		this->x = x;
		this->y = y;
		this->z = z;
		this->w = w;
	}

	float Vector4f::FindMagnitude(Vector4f* vec)
	{
		return sqrt(Dot(*vec, *vec));
	}

	Vector4f Vector4f::normalize()
	{
		return Vector4f(x / length, y / length, z / length, w / length);
	}

	float Vector4f::normalizeX()
	{
		return x / length;
	}

	float Vector4f::normalizeY()
	{
		return y / length;
	}

	float Vector4f::normalizeZ()
	{
		return z / length;
	}

	float Vector4f::normalizeW()
	{
		return w / length;
	}

	float Vector4f::Dot(Vector4f fvec, Vector4f svec)
	{
		return fvec.x * svec.x + fvec.y * svec.y + fvec.z * svec.z + fvec.w * svec.w;
	}

	Vector4f Vector4f::Lerp(Vector4f fvec, Vector4f svec, float percentage)
	{
		// Clamp To 1.0f
		percentage = percentage > 1.0f ? 1.0f : percentage;

		return Vector4f(
			fvec.x + (svec.x - fvec.x) * percentage,
			fvec.y + (svec.y - fvec.y) * percentage,
			fvec.z + (svec.z - fvec.z) * percentage,
			fvec.w + (svec.w - fvec.w) * percentage
		);
	}

	std::string Vector4f::to_string(Vector4f fvec)
	{
		return "(" + std::to_string(fvec.x) + ", " + std::to_string(fvec.y) + ", " + std::to_string(fvec.z) + ", " + std::to_string(fvec.w) + ")";
	}

	bool Vector4f::Equals(Vector4f fvec)
	{
		if (x == fvec.x && y == fvec.y && z == fvec.z && w == fvec.w) { return true; }
		else { return false; }
	}

	Vector4f Vector4f::operator+(Vector4f& val)
	{
		return Vector4f(x + val.x, y + val.y, z + val.z, w + val.w);
	}

	Vector4f Vector4f::operator-(Vector4f& val)
	{
		return Vector4f(x - val.x, y - val.y, z - val.z, w - val.w);
	}

	Vector4f Vector4f::operator/(Vector4f& val)
	{
		return Vector4f(x / val.x, y / val.y, z / val.z, w / val.w);
	}

	Vector4f Vector4f::operator*(Vector4f& val)
	{
		return Vector4f(x * val.x, y * val.y, z * val.z, w * val.w);
	}

	Vector4f Vector4f::operator*(float val)
	{
		return Vector4f(x * val, y * val, z * val, w * val);
	}

	Vector4f Vector4f::operator/(float val)
	{
		return Vector4f(x / val, y / val, z / val, w * val);
	}

	bool Vector4f::operator==(Vector4f* vec)
	{
		if (vec == this)
		{
			return true;
		}

		else { return false; }
	}

	Vector4f Vector4f::operator+(float val)
	{
		return Vector4f(x + val, y + val, z + val, w + val);
	}

	Vector4f Vector4f::operator-(float val)
	{
		return Vector4f(x - val, y - val, z - val, w - val);
	}
}