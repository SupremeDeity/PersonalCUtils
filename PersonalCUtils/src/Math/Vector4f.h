#pragma once

namespace PCU
{
	struct Vector4f
	{
	public:
		Vector4f(float x, float y, float z, float w);
		Vector4f();

		void Set(float x, float y, float z, float w);

		float FindMagnitude(Vector4f* vec);

		Vector4f normalize();
		float normalizeX();
		float normalizeY();
		float normalizeZ();
		float normalizeW();

		static float Dot(Vector4f fvec, Vector4f svec);

		static Vector4f Lerp(Vector4f fvec, Vector4f svec, float percentage);

		static std::string to_string(Vector4f fvec);

		// Checks if instances are same. refer to operator== for values.
		bool Equals(Vector4f fvec);

		Vector4f operator +(Vector4f& val);
		Vector4f operator -(Vector4f& val);
		Vector4f operator /(Vector4f& val);
		Vector4f operator *(Vector4f& val);

		Vector4f operator *(float val);
		Vector4f operator -(float val);
		Vector4f operator +(float val);
		Vector4f operator /(float val);

		// Checks if instances are same. refer to equals() for values.
		bool operator ==(Vector4f* vec);

	public:
		float x, y, z, w, length;
	};
}