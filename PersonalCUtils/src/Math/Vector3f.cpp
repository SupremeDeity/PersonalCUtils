#include "pcpch.h"
#include "Vector3f.h"

namespace PCU
{
	Vector3f::Vector3f(float x, float y, float z)
	{
		PCU_PROFILE_FUNCTION();
		this->x = x;
		this->y = y;
		this->z = z;

		this->length = FindMagnitude(this);
	}
	Vector3f::Vector3f() : x(0.0f), y(0.0f), z(0.0f), length(0.0f)
	{}

	void Vector3f::Set(float x, float y, float z)
	{
		this->x = x;
		this->y = y;
		this->z = z;
	}

	float Vector3f::FindMagnitude(Vector3f* vec)
	{
		PCU_PROFILE_FUNCTION();
		return sqrt(Dot(*vec, *vec));
	}

	Vector3f Vector3f::normalize()
	{
		return Vector3f(x / length, y / length, z / length);
	}

	float Vector3f::normalizeX()
	{
		return x / length;
	}

	float Vector3f::normalizeY()
	{
		return y / length;
	}

	float Vector3f::normalizeZ()
	{
		return z / length;
	}

	Vector3f Vector3f::Cross(Vector3f fvec, Vector3f svec)
	{
		return Vector3f(
			fvec.y * svec.z - fvec.z * svec.y,
			fvec.z * svec.x - fvec.x * svec.z,
			fvec.x * svec.y - fvec.y * svec.x
		);
	}

	float Vector3f::Dot(Vector3f fvec, Vector3f svec)
	{
		return fvec.x * svec.x + fvec.y * svec.y + fvec.z * svec.z;
	}

	Vector3f Vector3f::Lerp(Vector3f fvec, Vector3f svec, float percentage)
	{
		// Clamp To 1.0f
		percentage = percentage > 1.0f ? 1.0f : percentage;

		return Vector3f(
			fvec.x + (svec.x - fvec.x) * percentage,
			fvec.y + (svec.y - fvec.y) * percentage,
			fvec.z + (svec.z - fvec.z) * percentage
		);
	}

	std::string Vector3f::to_string(Vector3f fvec)
	{
		return ("(" + std::to_string(fvec.x) + ", " + std::to_string(fvec.y) + ", " + std::to_string(fvec.z) + ")");
	}

	bool Vector3f::Equals(Vector3f fvec)
	{
		if (x == fvec.x && y == fvec.y && z == fvec.z) { return true; }
		else { return false; }
	}

	bool Vector3f::operator==(Vector3f* vec)
	{
		if (vec == this)
		{
			return true;
		}

		else { return false; }
	}

	Vector3f Vector3f::operator+(Vector3f& val)
	{
		return Vector3f(x + val.x, y + val.y, z + val.z);
	}

	Vector3f Vector3f::operator-(Vector3f& val)
	{
		return Vector3f(x - val.x, y - val.y, z - val.z);
	}

	Vector3f Vector3f::operator/(Vector3f& val)
	{
		return Vector3f(x / val.x, y / val.y, z / val.z);
	}

	Vector3f Vector3f::operator*(Vector3f& val)
	{
		return Vector3f(x * val.x, y * val.y, z * val.z);
	}

	Vector3f Vector3f::operator*(float val)
	{
		return Vector3f(x * val, y * val, z * val);
	}

	Vector3f Vector3f::operator/(float val)
	{
		return Vector3f(x / val, y / val, z / val);
	}

	Vector3f Vector3f::operator+(float val)
	{
		return Vector3f(x + val, y + val, z + val);
	}

	Vector3f Vector3f::operator-(float val)
	{
		return Vector3f(x - val, y - val, z - val);
	}
}